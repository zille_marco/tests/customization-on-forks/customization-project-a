# This piece of code comes from the standard-project

This is a test to show how we can handle a `standard` repository in which all the code for the project, that can be shared among all the various customers, can be stored.

For each customer, we will create a fork, in which we can obviously make all the changes needed.

If, or when, for whatever reason, a feature developed on a fork is eligible to become available for every customer than we will cherry pick the commits from that very feature, and we'll merge them onto the standard project, so that every fork will be able to rebase and pull those changes.

# This change was introduced in customization-project-a

This is a test customization that won't get shared back to the `standard-project`

# This change was also introduced in customization-project-a

This is a test that instead will go back to the `standard-project` and get shared among all customizations!

# This is another change introduced in customization-project-a

This change also won't go back to the `standard-project`